﻿using System;
using System.Collections.Generic;
using System.IO;
using Crawler;
using ImdbCrawler;
using Newtonsoft.Json;
using MySql.Data.MySqlClient;

namespace StarPower
{
  class Program
  {
    private static NLog.Logger s_logger = NLog.LogManager.GetCurrentClassLogger();

    public static void Main(string[] args)
    {
      string config_file;
      if (args.Length == 1)
      {
        config_file = args[0];
      }
      else if (args.Length == 2)
      {
        config_file = args[1];
      }
      else
      {
        PrintUsage();
        return;
      }

      if (!File.Exists(config_file))
      {
        s_logger.Error("Could not locate config file {0}", config_file);
        return;
      }
      // @TODO: check permissions on file
      var config_contents = File.ReadAllText(config_file);
      Config config = JsonConvert.DeserializeObject<Config>(config_contents);
      string connectionStr = string.Format("server={0};database={1};user={2};password={3}", config.DbHost, config.Database, config.DbUser, config.DbPassword);
      MySqlConnection connection = new MySqlConnection(connectionStr);
      PersonStore.Conn = connection;
      MovieStore.Conn = connection;
      IList<Uri> seeds = new List<Uri>();

      if (!File.Exists(config.SeedFile))
      {
        s_logger.Error("Could not find seed file {0}", config.SeedFile);
        return;
      }
      
      if (!Directory.Exists(config.CacheDir))
      {
        s_logger.Error("Could not find cache directory {0}", config.CacheDir);
        return;
      }

      using (StreamReader reader = File.OpenText(config.SeedFile))
      {
        string line;
        while ((line = reader.ReadLine()) != null)
        {
          seeds.Add(new Uri(line));
        }
      }

      Crawler.Crawler crawler = new Crawler.Crawler(
        seeds,
        new RandomDelayPolicy(),
        new CachedPageFetcher(config.CacheDir),
        new List<IExtractor>(){ new ImdbPersonLinkExtractor(), new ImdbMovieLinkExtractor() },
        new List<IParser>(){ new ImdbPersonPageParser() , new ImdbMoviePageParser() }
      );
      crawler.Run();
    }

    private static void PrintUsage()
    {
      Console.WriteLine("usage: starpower <config file>");
    }
  }
}
