﻿namespace DAL
{
  public interface IDbConnection
  {
    void Open();
    void Close();
  }
}
