﻿using System.Collections.Generic;

namespace DAL
{
  public interface IDataStore<T>
  {
    T Get(string id);

    IEnumerable<T> GetAll();

    void Update(T t);
  }
}
