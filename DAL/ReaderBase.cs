﻿using System;
using System.Collections.Generic;

namespace DAL
{
  public abstract class ReaderBase<T>
  {
    public ReaderBase(Mapper<T> mapper, IDbConnection connection)
    {
      Mapper = mapper;
      Connection = connection;
    }

    public Mapper<T> Mapper { get; private set; }

    public IDbConnection Connection { get; private set; }

    protected string Statement { get; set; }

    public List<T> Execute()
    {
      throw new NotImplementedException();
    }
  }
}
