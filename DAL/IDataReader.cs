﻿namespace DAL
{
  public interface IDataReader
  {
    bool Read();
    void Close();
  }
}
