﻿using MySql.Data.MySqlClient;

namespace DAL
{
  public abstract class DbCommandBase
  {
    public DbCommandBase(string statement, MySqlConnection connection)
    {
      Statement = statement;
      Connection = connection;
    }

    public string Statement { get; private set; }

    public MySqlConnection Connection { get; private set; }
  }
}
