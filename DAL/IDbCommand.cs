﻿namespace DAL
{
  public interface IDbCommand
  {
    string Command { get; set; }
  }
}
