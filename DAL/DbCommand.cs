﻿using MySql.Data.MySqlClient;
using System;

namespace DAL
{
  public class DbCommand : DbCommandBase
  {
    private MySqlCommand m_command;

    public DbCommand(string stmt, MySqlConnection conn) : base(stmt, conn)
    {
      m_command = new MySqlCommand(stmt, conn);
    }

    public IDataReader ExecuteReader()
    {
      throw new NotImplementedException();
    }
  }
}
