﻿using MySql.Data.MySqlClient;
using System.Collections.Generic;

namespace DAL
{
  public abstract class Mapper<T>
  {
    public abstract T Map(MySqlDataReader reader);

    public T MapOne(MySqlDataReader reader)
    {
      T t = default(T);
      if (reader.Read())
      {
        t = Map(reader);
      }
      reader.Close();
      return t;
    }

    public IEnumerable<T> MapAll(MySqlDataReader reader)
    {
      List<T> data = new List<T>();

      while (reader.Read())
      {
        data.Add(Map(reader));
      }
      reader.Close();

      return data;
    }
  }
}
