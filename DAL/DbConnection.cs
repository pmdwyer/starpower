﻿using MySql.Data.MySqlClient;

namespace DAL
{
  public class DbConnection : IDbConnection
  {
    private MySqlConnection m_connection;

    public DbConnection(string server, string db, string user, string password)
    {
      string connection = string.Format("Server={0};User={1};Password={2};Database={3}",
                                        server, user, password, db);
      m_connection = new MySqlConnection(connection);
    }

    public void Close()
    {
      m_connection.Close();
    }

    public void Open()
    {
      m_connection.Open();
    }
  }
}
