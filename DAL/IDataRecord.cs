﻿using System.Collections.Generic;

namespace DAL
{
  public interface IDataRecord : IReadOnlyCollection<string>
  {
  }
}
