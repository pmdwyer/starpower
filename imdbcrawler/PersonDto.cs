using System;

namespace ImdbCrawler
{
  internal class PersonDto
  {
    public PersonDto()
    {
    }

    public PersonDto(Person p)
    {
      Id = -1;
      Name = p.Name;
      Url = p.Url;
      BirthDate = p.BirthDay;
      DeathDate = p.DeathDay;
      StarRating = p.Power;
    }

    public Person ToPerson()
    {
      Person p = new Person();
      p.Name = Name;
      p.Url = Url;
      p.BirthDay = BirthDate;
      p.DeathDay = DeathDate;
      p.Power = StarRating;
      return p;
    }

    public int Id { get; set; }

    public string Name { get; set; }

    public string Url { get; set; }

    public DateTime BirthDate { get; set; }

    public DateTime DeathDate { get; set; }

    public double StarRating { get; set; }
  }
}