﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using Crawler;

namespace ImdbCrawler
{
  public class ImdbPersonLinkExtractor : IExtractor
  {
    public IEnumerable<Uri> GetLinks(Page page)
    {
      List<Uri> links = new List<Uri>();
      var cast_anchors = page.doc.DocumentNode.SelectNodes("//table[@class=\"cast_list\"]/tr/td/a/@href");
      if (cast_anchors != null)
      {
        foreach (var anchor in cast_anchors)
        {
          Uri u = new Uri(page.uri, anchor.GetAttributeValue("href", ""));
          if (Matches(u))
            links.Add(u);
        }
      }
      return links;
    }

    private bool Matches(Uri u)
    {
      Regex re = new Regex("name/nm\\d+/");
      return re.IsMatch(u.AbsoluteUri);
    }
  }
}
