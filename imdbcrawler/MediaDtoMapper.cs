﻿using DAL;
using MySql.Data.MySqlClient;
using System;

namespace ImdbCrawler
{
  internal class MediaDtoMapper : Mapper<MediaDto>
  {
    public override MediaDto Map(MySqlDataReader reader)
    {
      MediaDto m = new MediaDto();
      m.Id = reader["media_id"] == DBNull.Value ? 0 : (int)reader["media_id"];
      m.Title = reader["title"] == DBNull.Value ? "" : (string)reader["title"];
      m.ReleaseDate = reader["release_date"] == DBNull.Value ? DateTime.MaxValue : (DateTime)reader["release_date"];
      m.Rating = reader["rating"] == DBNull.Value ? 0.0 : (double)reader["rating"];
      m.Votes = reader["votes"] == DBNull.Value ? 0 : (int)reader["votes"];
      m.BoxOffice = reader["box_office"] == DBNull.Value ? 0.0M : (decimal)reader["box_office"];
      m.Url = reader["url"] == DBNull.Value ? "" : (string)reader["url"];
      m.Format = reader["format"] == DBNull.Value ? "" : (string)reader["format"];
      return m;
    }
  }
}