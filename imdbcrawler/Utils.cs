using System;
using System.Globalization;
using System.Collections.Generic;
using System.Linq;

namespace ImdbCrawler
{
  internal class Utils
  {
    private static NLog.Logger s_logger = NLog.LogManager.GetCurrentClassLogger();

    internal static string[] MakeVariableParameters(int len)
    {
      string[] parameters = new string[len];
      for (int i = 0; i < len; i++)
      {
        parameters[i] = string.Format("@arg{0}", i);
      }
      return parameters;
    }

    internal static string Expand<T>(IEnumerable<T> vals)
    {
      return string.Join(",", vals.Select(v => string.Format("({0})", v)));
    }

    internal static int ParseInt(string toParse)
    {
      int val = 0;
      try
      {
        val = int.Parse(toParse, NumberStyles.AllowCurrencySymbol | NumberStyles.AllowThousands);
      }
      catch (Exception ex)
      {
        s_logger.Warn("Could not parse string {0} to int", toParse);
      }
      return val;
    }
  }
}