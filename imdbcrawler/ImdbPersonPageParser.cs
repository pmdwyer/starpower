﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

using Newtonsoft.Json;

using Crawler;

namespace ImdbCrawler
{
  public class ImdbPersonPageParser : IParser
  {
    private static NLog.Logger s_logger = NLog.LogManager.GetCurrentClassLogger();

    public bool CanParse(Page page)
    {
      Regex re = new Regex("name/nm\\d+/$");
      return re.IsMatch(page.uri.AbsolutePath);
    }

    public void Parse(Page page)
    {
      Person person = new Person();
      // name, url, birthday, deathday
      var nameNode = page.doc.DocumentNode.SelectSingleNode("//div[@id=\"name-overview-widget\"]//h1/span");
      person.Name = nameNode.InnerText.Trim();
      person.Url = page.uri.LocalPath;

      // bday cannot bet null
      var bdayNode = page.doc.DocumentNode.SelectSingleNode("//div[@id=\"name-born-info\"]//time");
      if (bdayNode == null)
      {
        s_logger.Warn("Found person without birthday: '{0}'", person.Name);
        return;
      }
      string bday = bdayNode.GetAttributeValue("datetime", "");
      try
      {
        person.BirthDay = GetDate(bday);
      }
      catch (ArgumentOutOfRangeException ex)
      {
        s_logger.Warn("Could not parse date {}", bday);
        return;
      }

      var ddayNode = page.doc.DocumentNode.SelectSingleNode("//div[@id=\"name-death-info\"]//time");
      if (ddayNode != null)
      {
        string dday = ddayNode.GetAttributeValue("datetime", "");
        if (dday != "")
        {
          try
          {
            person.DeathDay = GetDate(dday);
          }
          catch (System.Exception)
          {
            s_logger.Warn("Could not parse date {}", bday);
            return;
          }
        }
      }

      var films = new HashSet<string>();
      var filmographyNodes = page.doc.DocumentNode.SelectNodes("//div[starts-with(@id, \"actor-tt\")] | //div[starts-with(@id, \"actress-tt\")]");
      if (filmographyNodes != null)
      {
        foreach (var filmographyNode in filmographyNodes)
        {
          var texts = filmographyNode.SelectNodes("./text()");
          if (texts.Any(node => node.InnerText.Contains("(")))
            continue;

          var filmAnchors = filmographyNode.SelectNodes(".//a");
          foreach (var filmNode in filmAnchors)
          {
            // @TODO: generisize filters
            if (filmNode.InnerText.Contains("Episode"))
              continue;
            films.Add(filmNode.InnerText.Trim());
          }
        }
      }
      else
      {
        s_logger.Warn("No film nodes for {0} at {1}", person.Name, person.Url);
      }
      
      var knownForAnchors = page.doc.DocumentNode.SelectNodes("//div[@class=\"knownfor-title-role\"]/a");
      if (knownForAnchors != null)
      {
        foreach (var anchor in knownForAnchors)
        {
          films.Add(anchor.InnerText.Trim());
        }
      }
      else
      {
        s_logger.Warn("No known for nodes for {0} at {1}", person.Name, person.Url);
      }
      
      person.Films = new List<string>(films);
      PersonStore.Instance.Update(person);

      s_logger.Info("Person {0}", person);
    }

    private DateTime GetDate(string date)
    {
      string[] ymd = date.Split('-');
      var year = int.Parse(ymd[0]);
      var month = int.Parse(ymd[1]);
      var day = int.Parse(ymd[2]);
      return new DateTime(year, month, day);
    }
  }
}