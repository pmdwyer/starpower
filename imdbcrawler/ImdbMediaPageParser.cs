﻿using System;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;

using HtmlAgilityPack;
using MySql.Data.MySqlClient;
using Newtonsoft.Json;

using Crawler;

namespace ImdbCrawler
{
  public class ImdbMediaPageParser : IParser
  {
    private static NLog.Logger s_logger = NLog.LogManager.GetCurrentClassLogger();

    // @TODO: clean this up, use interfaces
    public ImdbMediaPageParser()
    {
    }

    public bool CanParse(Page page)
    {
      // @TODO: static regex
      Regex re = new Regex("title/tt\\d+\\/$");
      return re.IsMatch(page.uri.AbsolutePath);
    }

    public void Parse(Page page)
    {
      Movie movie = new Movie();
      // title, release date, cast, director, writer, rating, votes, budget, box-office, url
      var title = page.doc.DocumentNode.SelectSingleNode("//div[@class=\"title_wrapper\"]/h1").InnerText;
      movie.Title = HtmlEntity.DeEntitize(title.Trim());
      movie.Url = page.uri.LocalPath;

      var releaseDateNode = page.doc.DocumentNode.SelectSingleNode("//h4[text()=\"Release Date:\"]");
      if (releaseDateNode == null)
      {
        // bail if there's no release date
        return;
      }
      string release = releaseDateNode.ParentNode.InnerText.Replace("Release Date:", "").Trim();
      try
      {
        string[] words = release.Split();
        string date = string.Join(" ", words.Take(3));
        movie.ReleaseDate = DateTime.Parse(date);
      }
      catch (Exception ex)
      {
        s_logger.Error(ex);
        s_logger.Warn("Could not parse release date {0}", release);
        return;
      }

      var scoreNode = page.doc.DocumentNode.SelectSingleNode("//span[@itemprop=\"ratingValue\"]");
      if (scoreNode != null)
      {
        movie.Rating = Convert.ToDouble(scoreNode.InnerText.Trim());
        if (movie.Rating <= 0.0)
        {
          // bail on a correctly parsed 0 rating
          return;
        }
      }
      else
      {
        // bail on no score
        return;
      }

      var votesNode = page.doc.DocumentNode.SelectSingleNode("//span[@itemprop=\"ratingCount\"]");
      if (votesNode != null)
      {
        movie.Votes = Utils.ParseInt(votesNode.InnerText.Trim());
        if (movie.Votes == 0)
        {
          // bail on a correctly parsed 0 votes
          return;
        }
      }
      else
      {
        // bail on no votes
        return;
      }

      var budgetNode = page.doc.DocumentNode.SelectSingleNode("//h4[. = \"Budget:\"]");
      if (budgetNode != null)
      {
        var toParse = budgetNode.ParentNode.InnerText.Replace("Budget:", "").Trim();
        toParse = toParse.Replace("(estimated)", "").Trim();
        movie.Budget = Utils.ParseInt(toParse.Trim());
      }

      var globalBoxOfficeNode = page.doc.DocumentNode.SelectSingleNode("//h4[. = \"Cumulative Worldwide Gross:\"]");
      if (globalBoxOfficeNode != null)
      {
        var toParse = globalBoxOfficeNode.ParentNode.InnerText.Replace("Cumulative Worldwide Gross:", "").Trim();
        movie.BoxOffice = Utils.ParseInt(toParse);
      }

      var credit_summary_items = page.doc.DocumentNode.SelectNodes("//div[@class=\"credit_summary_item\"]");
      if (credit_summary_items == null)
      {
        s_logger.Warn("Found page with no credits: '{0}'", movie.Title);
        return;
      }

      foreach (var item in credit_summary_items)
      {
        var heading = item.SelectSingleNode(".//h4");
        if (heading == null)
        {
          continue;
        }

        var anchors = item.SelectNodes(".//a");
        if (heading.InnerText == "Director:" || heading.InnerText == "Directors:" || heading.InnerText == "Writers:")
        {
          foreach (var a in anchors)
          {
            if (a.InnerText.Contains("more credits"))
            {
              continue;
            }
            movie.People.Add(HtmlEntity.DeEntitize(a.InnerHtml.Trim()));
          }
        }
      }
      
      var castNodes = page.doc.DocumentNode.SelectNodes("//table[@class=\"cast_list\"]/tr/td[2]/a");
      foreach (var actorNode in castNodes)
      {
        movie.People.Add(actorNode.InnerText.Trim());
      }

      MovieStore.Instance.Update(movie);
      s_logger.Info("Movie {0}", movie);
    }
  }
}
