﻿using System;
using System.Collections.Generic;
using System.Linq;
using MySql.Data.MySqlClient;
using DAL;

namespace ImdbCrawler
{
  public sealed class PersonStore : IDataStore<Person>
  {
    private static NLog.Logger s_logger = NLog.LogManager.GetCurrentClassLogger();
    private static PersonDtoMapper s_mapper = new PersonDtoMapper();

    public static MySqlConnection Conn { get; set; }
    private static MySqlConnection m_conn;
    
    public static PersonStore Instance
    {
      get
      {
        if (instance == null)
        {
          instance = new PersonStore(Conn);
        }
        return instance;
      }
    }
    private static PersonStore instance = null;

    private PersonStore(MySqlConnection conn)
    {
      m_conn = Conn;
    }

    public Person Get(string name)
    {
      s_logger.Info("Retrieving person {0}", name);
      return Fetch(name).ToPerson();
    }

    public IEnumerable<Person> GetAll()
    {
      s_logger.Info("Retreiving all people");
      return FetchAll().Select(dto => dto.ToPerson());
    }

    public IEnumerable<double> GetAllMovieRatingsFor(string name)
    {
      PersonDto dto = Fetch(name);
      if (dto == null)
      {
        return null;
      }
      m_conn.Open();
      List<double> ratings = new List<double>();
      MySqlCommand cmd = new MySqlCommand(SelectRatings, m_conn);
      cmd.Parameters.AddWithValue("@id", dto.Id);
      MySqlDataReader reader = cmd.ExecuteReader();
      while (reader.Read())
      {
        if (reader["rating"] != DBNull.Value)
          ratings.Add((double)reader["rating"]);
      }
      m_conn.Close();
      return ratings;
    }

    public void Update(Person p)
    {
      s_logger.Info("Storing person {0}", p);
      PersonDto dto = Fetch(p.Name);
      if (dto == null)
      {
        Insert(p);
        dto = Fetch(p.Name);
      }
      dto.DeathDate = p.DeathDay;
      dto.Url = p.Url;
      if (p.Power > 0.0)
      {
        dto.StarRating = p.Power;
      }
      Store(dto);
      IEnumerable<int> filmIds = MovieStore.Instance.GetAllIds(p.Films);
      UpdateStarsIn(dto.Id, filmIds);
    }

    internal IEnumerable<PersonDto> FetchAll()
    {
      m_conn.Open();
      IEnumerable<PersonDto> people = null;
      MySqlCommand cmd = new MySqlCommand(SelectAll, m_conn);
      MySqlDataReader reader = cmd.ExecuteReader();
      people = s_mapper.MapAll(reader);
      m_conn.Close();
      return people;
    }

    internal PersonDto Fetch(string name)
    {
      m_conn.Open();
      MySqlCommand cmd = new MySqlCommand(SelectOne, m_conn);
      cmd.Parameters.AddWithValue("@name", "%" + name + "%");
      MySqlDataReader reader = cmd.ExecuteReader();
      PersonDto p = s_mapper.MapOne(reader);
      m_conn.Close();
      return p;
    }

    internal IEnumerable<int> FetchIds(IEnumerable<string> names)
    {
      m_conn.Open();
      string[] parameters = Utils.MakeVariableParameters(names.Count());
      string SelectIn = string.Format(SelectInFormat, string.Join(",", parameters));
      MySqlCommand cmd = new MySqlCommand(SelectIn, m_conn);
      for (int i = 0; i < parameters.Length; i++)
      {
        cmd.Parameters.AddWithValue(parameters[i], names.ElementAt(i));
      }
      MySqlDataReader reader = cmd.ExecuteReader();
      List<int> ids = new List<int>();
      while (reader.Read())
      {
        if (reader["person_id"] != DBNull.Value)
        {
          ids.Add((int)reader["person_id"]);
        }
      }
      reader.Close();
      m_conn.Close();
      return ids;
    }

    internal bool UpdateStarsIn(int personId, IEnumerable<int> filmIds)
    {
      if (filmIds == null || filmIds.Count() <= 0)
      {
        return true;
      }
      bool completed = true;
      string InsertStarsIn = string.Format(InsertStarsInFormat,
        string.Join(",",
          filmIds.Select(fid => string.Format("({0},{1})", personId, fid))));
      m_conn.Open();
      MySqlTransaction transaction = m_conn.BeginTransaction();
      try
      {
        MySqlCommand cmd = new MySqlCommand(InsertStarsIn, m_conn);
        cmd.Transaction = transaction;
        cmd.ExecuteNonQuery();
        transaction.Commit();
      }
      catch (MySqlException ex)
      {
        s_logger.Error(ex);
        transaction.Rollback();
        completed = false;
      }
      finally
      {
        m_conn.Close();
      }
      return completed;
    }

    internal bool Store(PersonDto dto)
    {
      if (dto.Id < 0)
      {
        return StoreByName(dto);
      }
      else
      {
        return StoreById(dto);
      }
    }

    internal bool StoreByName(PersonDto dto)
    {
      bool success = false;
      m_conn.Open();

      MySqlTransaction transaction = m_conn.BeginTransaction();
      try
      {
        MySqlCommand cmd = new MySqlCommand(UpdateByName, m_conn);
        cmd.Transaction = transaction;
        cmd.Parameters.AddWithValue("@name", dto.Name);
        cmd.Parameters.AddWithValue("@url", dto.Url);
        cmd.Parameters.AddWithValue("@bday", dto.BirthDate);
        cmd.Parameters.AddWithValue("@deathdate", dto.DeathDate);
        cmd.Parameters.AddWithValue("@power", dto.StarRating);
        cmd.ExecuteNonQuery();
        transaction.Commit();
        success = true;
      }
      catch (MySqlException ex)
      {
        s_logger.Error(ex);
        transaction.Rollback();
      }
      finally
      {
        m_conn.Close();
      }
      return success;
    }

    internal bool StoreById(PersonDto dto)
    {
      bool success = true;
      m_conn.Open();

      MySqlTransaction transaction = m_conn.BeginTransaction();
      try
      {
        MySqlCommand cmd = new MySqlCommand(UpdateById, m_conn);
        cmd.Transaction = transaction;
        cmd.Parameters.AddWithValue("@id", dto.Id);
        cmd.Parameters.AddWithValue("@name", dto.Name);
        cmd.Parameters.AddWithValue("@url", dto.Url);
        cmd.Parameters.AddWithValue("@bday", dto.BirthDate);
        cmd.Parameters.AddWithValue("@deathdate", dto.DeathDate);
        cmd.Parameters.AddWithValue("@power", dto.StarRating);
        cmd.ExecuteNonQuery();
        transaction.Commit();
      }
      catch (MySqlException ex)
      {
        s_logger.Error(ex);
        transaction.Rollback();
        success = false;
      }
      finally
      {
        m_conn.Close();
      }
      return success;
    }

    internal bool Insert(Person person)
    {
      bool completed = true;
      m_conn.Open();

      MySqlTransaction transaction = m_conn.BeginTransaction();
      try
      {
        MySqlCommand cmd = new MySqlCommand(InsertPerson, m_conn);
        cmd.Transaction = transaction;
        cmd.Parameters.AddWithValue("@name", person.Name);
        cmd.Parameters.AddWithValue("@bday", person.BirthDay);
        cmd.ExecuteNonQuery();
        transaction.Commit();
      }
      catch (MySqlException ex)
      {
        s_logger.Error(ex);
        transaction.Rollback();
        completed = false;
      }
      finally
      {
        m_conn.Close();
      }
      return completed;
    }

    private const string UpdateById = "UPDATE people SET name = @name, url = @url, birthdate = @bday, deathdate = @deathdate, star_rating = @power WHERE person_id = @id";
    private const string UpdateByName = "UPDATE people SET url = @url, deathdate = @deathdate, star_rating = @power WHERE name = @name and birthdate = @bday";
    private const string SelectAll = "SELECT * FROM people";
    private const string SelectOne = "SELECT * FROM people WHERE `name` LIKE @name";
    private const string SelectInFormat = "SELECT * FROM people WHERE name IN ({0})";
    private const string InsertPerson = "INSERT INTO people (name, birthdate) VALUES (@name,@bday)";
    private const string InsertStarsInFormat = "INSERT IGNORE INTO stars_in (person, movie) VALUES {0}";
    private const string SelectRatings = "SELECT m.rating FROM movies as m JOIN (people, stars_in) ON (people.person_id  = stars_in.person and m.movie_id = stars_in.movie) WHERE people.person_id = @id";
  }
}
