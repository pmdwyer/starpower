namespace ImdbCrawler
{
  public enum MediaFormat
  {
    Movie,
    TvShow,
    TvEpisode,
    Webisode,
    Game,
    None
  }
}