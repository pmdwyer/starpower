using System;

namespace ImdbCrawler
{
  internal class MediaDto
  {
    public MediaDto()
    {
    }

    public MediaDto(Media m)
    {
      Id = -1;
      Title = m.Title;
      ReleaseDate = m.ReleaseDate;
      Rating = m.Rating;
      Votes = m.Votes;
      Budget = m.Budget;
      BoxOffice = m.BoxOffice;
      Url = m.Url;
      Format = m.Format.ToString();
    }

    public Media ToMedia()
    {
      Media m = new Media();
      m.Title = Title;
      m.ReleaseDate = ReleaseDate;
      m.Rating = Rating;
      m.Votes = Votes;
      m.Budget = Budget;
      m.BoxOffice = BoxOffice;
      m.Url = Url;
      m.Format = MediaFormat.None;
      MediaFormat format;
      if (Enum.TryParse(Format, out format))
      {
        m.Format = format;
      }
      return m;
    }

    public int Id { get; set; }

    public string Title { get; set; }

    public DateTime ReleaseDate { get; set; }

    public double Rating { get; set; }

    public int Votes { get; set; }

    public decimal Budget { get; set; }

    public decimal BoxOffice { get; set; }

    public string Url { get; set; }

    public string Format { get; set; }
  }
}