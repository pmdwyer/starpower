﻿using System;
using System.Collections.Generic;

namespace ImdbCrawler
{
  public class Media
  {
    public string Url { get; set; }

    public string Title { get; set; }

    public DateTime ReleaseDate { get; set; }

    // don't use ids or person objects, force a lookup by name
    public IList<string> People { get; set; } = new List<string>();

    public double Rating { get; set; }

    public int Votes { get; set; }

    public decimal Budget { get; set; }

    public decimal BoxOffice { get; set; }

    public MediaFormat Format { get; set; }

    public override string ToString()
    {
      return string.Format("{{title: {0}, release_date: {6}, rating: {1}, votes: {2}, budget: {3}, box_office: {4}, people: [{5}]}}",
        Title,
        Rating,
        Votes,
        Budget,
        BoxOffice,
        string.Join(",", People),
        ReleaseDate);
    }
  }
}
