﻿using System;
using System.Collections.Generic;

namespace ImdbCrawler
{
  // @TODO: json serialization/deserialization
  public class Person
  {
    public string Name { get; set; }

    public string Url { get; set; }

    public DateTime BirthDay { get; set; } = new DateTime();

    public DateTime DeathDay { get; set; } = new DateTime();

    public double Power { get; set; }

    // don't use ids or person objects, force a lookup by title
    public IList<string> Films { get; set; } = new List<string>();

    // @TODO: use json formatting
    public override string ToString()
    {
      return String.Format("{{name: \"{0}\", birthdate: {1}, deathdate: {2}, url: \"{3}\", power: {4}, films: [{5}]}}",
        Name,
        BirthDay,
        DeathDay,
        Url,
        Power,
        string.Join(",", Films));
    }
  }
}