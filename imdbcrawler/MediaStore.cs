﻿using System;
using System.Collections.Generic;
using System.Linq;
using MySql.Data.MySqlClient;

namespace ImdbCrawler
{
  public class MediaStore
  {
    private static NLog.Logger s_logger = NLog.LogManager.GetCurrentClassLogger();
    private static MediaDtoMapper s_mapper = new MediaDtoMapper();

    public static MySqlConnection Conn { get; set; }

    private static MySqlConnection _conn;

    public static MediaStore Instance
    {
      get
      {
        if (instance == null)
        {
          instance = new MediaStore(Conn);
        }
        return instance;
      }
    }

    private static MediaStore instance = null;

    private MediaStore(MySqlConnection conn)
    {
      _conn = conn;
    }

    public Media Get(string title)
    {
      s_logger.Info("Getting media with title {0}", title);
      return Fetch(title).ToMedia();
    }

    public IEnumerable<Media> GetAll()
    {
      s_logger.Info("Getting all media");
      return FetchAll().Select(dto => dto.ToMedia());
    }

    public void AddTitles(IEnumerable<string> titles)
    {
      var newTitles = GetMissingTitles(titles);
      InsertAll(newTitles);
    }

    public void Update(Media m)
    {
      s_logger.Info("Updating media with title {0}", m.Title);
      MediaDto dto = Fetch(m.Title);
      if (dto == null)
      {
        Insert(m);
        dto = Fetch(m.Title);
      }
      dto.Budget = m.Budget;
      dto.BoxOffice = m.BoxOffice;
      dto.Rating = m.Rating;
      dto.Url = m.Url;
      dto.Votes = m.Votes;
      Store(dto);
      IEnumerable<int> peopleIds = PersonStore.Instance.FetchIds(m.People);
      UpdateStarsIn(dto.Id, peopleIds);
    }

    internal bool UpdateStarsIn(int filmId, IEnumerable<int> peopleIds)
    {
      if (peopleIds.Count() <= 0)
      {
        return true;
      }
      bool completed = true;
      _conn.Open();

      string InsertStarsIn = string.Format(InsertStarsInFormat, 
        string.Join(",",
          peopleIds.Select(pid => string.Format("({0},{1})", pid, filmId))));

      MySqlTransaction transaction = _conn.BeginTransaction();
      try
      {
        MySqlCommand cmd = new MySqlCommand(InsertStarsIn, _conn);
        cmd.Transaction = transaction;
        cmd.ExecuteNonQuery();
        transaction.Commit();
      }
      catch (MySqlException ex)
      {
        s_logger.Error(ex);
        transaction.Rollback();
        completed = false;
      }
      finally
      {
        _conn.Close();
      }
      return completed;
    }

    internal bool Insert(Media media)
    {
      bool completed = true;
      _conn.Open();

      MySqlTransaction transaction = _conn.BeginTransaction();
      try
      {
        MySqlCommand cmd = new MySqlCommand(InsertMedia, _conn);
        cmd.Transaction = transaction;
        cmd.Parameters.AddWithValue("@title", media.Title);
        cmd.Parameters.AddWithValue("@releasedate", media.ReleaseDate);
        cmd.ExecuteNonQuery();
        transaction.Commit();
      }
      catch (MySqlException ex)
      {
        s_logger.Error(ex);
        transaction.Rollback();
        completed = false;
      }
      finally
      {
        _conn.Close();
      }
      return completed;
    }

    internal IEnumerable<int> GetAllIds(IEnumerable<string> media)
    {
      if (media.Count() <= 0)
      {
        return null;
      }
      _conn.Open();
      string[] parameters = Utils.MakeVariableParameters(media.Count());
      string SelectAllIds = string.Format(SelectAllIdsFormat, string.Join(",", parameters));
      MySqlCommand cmd = new MySqlCommand(SelectAllIds, _conn);
      for (int i = 0; i < parameters.Length; i++)
      {
        cmd.Parameters.AddWithValue(parameters[i], media.ElementAt(i));
      }
      MySqlDataReader reader = cmd.ExecuteReader();
      List<int> ids = new List<int>();
      while (reader.Read())
      {
        if (reader["media_id"] != DBNull.Value)
        {
          ids.Add((int)reader["media_id"]);
        }
      }
      reader.Close();
      _conn.Close();
      return ids;
    }

    internal IEnumerable<string> GetMissingTitles(IEnumerable<string> titles)
    {
      s_logger.Info("Retrieving missing titles");
      return FetchNonExisting(titles);
    }

    internal IEnumerable<MediaDto> FetchAll()
    {
      _conn.Open();
      IEnumerable<MediaDto> media = null;
      MySqlCommand cmd = new MySqlCommand(SelectAll, _conn);
      MySqlDataReader reader = cmd.ExecuteReader();
      media = s_mapper.MapAll(reader);
      _conn.Close();
      return media;
    }

    internal MediaDto Fetch(string title)
    {
      _conn.Open();
      MySqlCommand cmd = new MySqlCommand(SelectTitle, _conn);
      cmd.Parameters.AddWithValue("@title", "%" + title + "%");
      MySqlDataReader reader = cmd.ExecuteReader();
      MediaDto m = s_mapper.MapOne(reader);
      _conn.Close();
      return m;
    }

    internal IEnumerable<string> FetchNonExisting(IEnumerable<string> titles)
    {
      string[] parameters = Utils.MakeVariableParameters(titles.Count());
      string SelectNotIn = string.Format(SelectNotInFormat, "VALUES " + Utils.Expand<string>(parameters));
      _conn.Open();
      MySqlCommand cmd = new MySqlCommand(SelectNotIn, _conn);
      for (int i = 0; i < titles.Count(); i++)
      {
        cmd.Parameters.AddWithValue(parameters[i], titles.ElementAt(i));
      }
      MySqlDataReader reader = cmd.ExecuteReader();
      List<string> missingTitles = new List<string>();
      while (reader.Read())
      {
        if (reader["title"] != DBNull.Value)
          missingTitles.Add((string)reader["title"]);
      }
      reader.Close();
      _conn.Close();
      return missingTitles;
    }

    internal bool Store(MediaDto media)
    {
      if (media.Id < 0)
      {
        return StoreByTitle(media);
      }
      else
      {
        return StoreById(media);
      }
    }

    internal bool StoreByTitle(MediaDto media)
    {
      _conn.Open();

      MySqlTransaction transaction = _conn.BeginTransaction();
      bool completed = true;
      try
      {
        MySqlCommand cmd = new MySqlCommand(UpdateByTitle, _conn);
        cmd.Transaction = transaction;
        cmd.Parameters.AddWithValue("@title", media.Title);
        cmd.Parameters.AddWithValue("@releasedate", media.ReleaseDate);
        cmd.Parameters.AddWithValue("@votes", media.Votes);
        cmd.Parameters.AddWithValue("@rating", media.Rating);
        cmd.Parameters.AddWithValue("@boxoffice", media.BoxOffice);
        cmd.ExecuteNonQuery();
        transaction.Commit();
      }
      catch (MySqlException ex)
      {
        s_logger.Error(ex);
        transaction.Rollback();
        completed = false;
      }
      finally
      {
        _conn.Close();
      }
      return completed;
    }

    internal bool StoreById(MediaDto media)
    {
      _conn.Open();

      MySqlTransaction transaction = _conn.BeginTransaction();
      bool completed = true;
      try
      {
        MySqlCommand cmd = new MySqlCommand(UpdateById, _conn);
        cmd.Transaction = transaction;
        cmd.Parameters.AddWithValue("@id", media.Id);
        cmd.Parameters.AddWithValue("@title", media.Title);
        cmd.Parameters.AddWithValue("@releasedate", media.ReleaseDate);
        cmd.Parameters.AddWithValue("@votes", media.Votes);
        cmd.Parameters.AddWithValue("@rating", media.Rating);
        cmd.Parameters.AddWithValue("@boxoffice", media.BoxOffice);
        cmd.Parameters.AddWithValue("@budget", media.Budget);
        cmd.Parameters.AddWithValue("@url", media.Url);
        cmd.ExecuteNonQuery();
        transaction.Commit();
      }
      catch (MySqlException ex)
      {
        s_logger.Error(ex);
        transaction.Rollback();
        completed = false;
      }
      finally
      {
        _conn.Close();
      }
      return completed;
    }

    internal bool InsertAll(IEnumerable<string> media)
    {
      if (media.Count() <= 0)
      {
        return true;
      }
      
      bool completed = true;
      string[] parameters = Utils.MakeVariableParameters(media.Count());
      string InsertAll = string.Format(InsertAllFormat, "VALUES " + Utils.Expand<string>(parameters));
      _conn.Open();
      try
      {
        MySqlTransaction transaction = _conn.BeginTransaction();
        MySqlCommand cmd = new MySqlCommand(InsertAll, _conn);
        cmd.Transaction = transaction;
        for (int i = 0; i < parameters.Length; i++)
        {
          cmd.Parameters.AddWithValue(parameters[i], media.ElementAt(i));
        }
        cmd.ExecuteNonQuery();
        transaction.Commit();
      }
      catch (MySqlException ex)
      {
        s_logger.Error(ex);
        completed = false;
      }
      finally
      {
        _conn.Close();
      }
      return completed;
    }

    private const string SelectAll = "SELECT * FROM media";
    private const string SelectTitle = "SELECT * FROM media WHERE title LIKE @title";
    private const string SelectId = "SELECT * FROM media AS m JOIN stars_in s ON (m.media_id = s.media) JOIN people p ON (p.person_id = s.person) WHERE s.person = @id";
    private const string SelectNotInFormat = "WITH m(title) AS ({0}) SELECT m.title FROM m EXCEPT (SELECT title FROM media)";
    private const string SelectAllIdsFormat = "SELECT media_id FROM media WHERE title IN ({0})";
    private const string UpdateById = "UPDATE media SET title = @title, release_date = @releasedate, votes = @votes, rating = @rating, box_office = @boxoffice, budget = @budget, url = @url WHERE media_id = @id";
    private const string UpdateByTitle = "UPDATE media SET votes = @votes, rating = @rating, box_office = @boxoffice, budget = @budget, url = @url WHERE title = @title and release_date = @releasedate";
    private const string InsertAllFormat = "INSERT INTO media (title) {0}";
    private const string InsertMedia = "INSERT INTO media (title, release_date) VALUES (@title, @releasedate)";
    private const string InsertStarsInFormat = "INSERT IGNORE INTO stars_in (person, media) VALUES {0}";
  }
}
