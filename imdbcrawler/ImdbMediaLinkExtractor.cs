﻿using System;
using System.Collections.Generic;
using HtmlAgilityPack;
using Crawler;
using System.Text.RegularExpressions;

namespace ImdbCrawler
{
  public class ImdbMediaLinkExtractor : IExtractor
  {
    public IEnumerable<Uri> GetLinks(Page page)
    {
      List<Uri> links = new List<Uri>();
      var film_anchors = page.doc.DocumentNode.SelectNodes("//div[starts-with(@id, \"actor-tt\")] | //div[starts-with(@id, \"actress-tt\")]/b/a");
      if (film_anchors != null)
      {
        foreach (var anchor in film_anchors)
        {
          var filmAnchor = anchor.SelectSingleNode(".//a");
          if (filmAnchor == null)
            continue;
          Uri u = new Uri(page.uri, filmAnchor.GetAttributeValue("href", ""));
          if (Matches(u))
            links.Add(u);
        }
      }
      return links;
    }

    private bool Matches(Uri u)
    {
      Regex re = new Regex("title/tt\\d+/");
      return re.IsMatch(u.AbsoluteUri);
    }
  }
}