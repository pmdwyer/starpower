﻿using DAL;
using MySql.Data.MySqlClient;
using System;

namespace ImdbCrawler
{
  internal class PersonDtoMapper : Mapper<PersonDto>
  {
    public override PersonDto Map(MySqlDataReader reader)
    {
      PersonDto dto = new PersonDto();
      dto.Id = reader["person_id"] == DBNull.Value ? 0 : (int)reader["person_id"];
      dto.Name = reader["name"] == DBNull.Value ? "" : (string)reader["name"];
      dto.Url = reader["url"] == DBNull.Value ? "" : (string)reader["url"];
      dto.BirthDate = reader["birthdate"] == DBNull.Value ? DateTime.Today : (DateTime)reader["birthdate"];
      dto.DeathDate = reader["deathdate"] == DBNull.Value ? DateTime.Today : (DateTime)reader["deathdate"];
      dto.StarRating = reader["star_rating"] == DBNull.Value ? -1.0 : (double)reader["star_rating"];
      return dto;
    }
  }
}
