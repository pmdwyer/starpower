CREATE DATABASE IF NOT EXISTS `starpower`;

USE `starpower`;

SET FOREIGN_KEY_CHECKS = 0;

CREATE OR REPLACE TABLE `stars_in` (
  `person` INT,
  `media` INT,
  PRIMARY KEY (`person`, `media`),
  CONSTRAINT `fk_stars_in_person` FOREIGN KEY (`person`) REFERENCES `people` (`person_id`),
  CONSTRAINT `fk_stars_in_media` FOREIGN KEY (`media`) REFERENCES `media` (`media_id`)
) ENGINE = InnoDB;

CREATE OR REPLACE TABLE `people` (
  `person_id` INT AUTO_INCREMENT PRIMARY KEY,
  `name` VARCHAR(128) NOT NULL,
  `birthdate` DATE,
  `deathdate` DATE,
  `url` VARCHAR(128)
) ENGINE = InnoDB;

CREATE OR REPLACE TABLE `media` (
  `media_id` INT AUTO_INCREMENT PRIMARY KEY,
  `title` VARCHAR(128) NOT NULL,
  `release_date` DATE,
  `rating` REAL,
  `votes` INT,
  `budget` DECIMAL(65, 30),
  `box_office` DECIMAL(65, 30),
  `url` VARCHAR(128),
  `format` VARCHAR(32)
) ENGINE = InnoDB;

CREATE OR REPLACE TABLE `ratings` (
  `person` INT PRIMARY KEY,
  `avg_rating` REAL,
  `mean_rating` REAL,
  `bayesian_rating` REAL,
  CONSTRAINT `fk_ratings_person` FOREIGN KEY (`person`) REFERENCES `people` (`person_id`)
) ENGINE = InnoDB;

SET FOREIGN_KEY_CHECKS = 1;

SHOW TABLES;

SELECT * FROM people;
SELECT * FROM media;
SELECT * FROM stars_in;
SELECT * FROM ratings;