USE `starpower`;

INSERT INTO `people` (`url`, `name`, `birthdate`) VALUES ('/name/nm0788335/', 'Michael Shannon', '1974-08-07'), ('/name/nm0000130/', 'Jamie Lee Curtis', '1958-11-22'), ('/name/nm1869101/', 'Ana de Armas', '1988-04-30'), ('/name/nm0262635/', 'Chris Evans', '1981-06-13'), ('/name/nm0185819/', 'Daniel Craig', '1968-03-02');
-- DELETE FROM people WHERE `person_id` BETWEEN 2 AND 6;
SELECT * FROM people;

INSERT INTO `movies` (`title`, `votes`, `rating`) VALUES ("The Shape of Water", 349148, 7.3), ("True Lies", 229748, 7.2), ("Blade Runner 2049", 429134, 8.0), ("The Avengers", 1230430, 8.0), ("Casino Royale", 563694, 8.0), ("Knives Out", 298870, 7.9);
-- DELETE FROM movies WHERE `movie_id` BETWEEN 7 AND 12;
SELECT * FROM movies;

INSERT INTO `stars_in` (`person`, `movie`) VALUES (7, 1), (7, 6), (8, 2), (8, 6), (9, 3), (9, 6), (10, 4), (10, 6), (11, 5), (11, 6);
SELECT * FROM stars_in;