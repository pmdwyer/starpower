import Vue from 'vue'
import Vuex from 'vuex'
import actors from './modules/actors'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
  },
  mutations: {
  },
  actions: {
  },
  modules: {
    actors
  }
})
