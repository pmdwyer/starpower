import axios from 'axios';

const state = {
  actors:[]
};

const getters = {
  allActors: (state: any) => state.actors,
  sortedActors: (state: any) => {
    return state.actors.sort((a1: any, a2: any) => {
      if (a1.power > a2.power)
        return -1;
      if (a2.power > a1.power)
        return 1;
      return 0;
    });
  },
  getActor: (state: any) => (name: string) => {
    return state.actors.find((a: any) => a.name === name);
  }
};

const actions = {
  async fetchActors(val: any) {
    const response = await axios.get('http://localhost:8081/actor');
    val.commit('setActors', response.data.data);
  }
};

const mutations = {
  setActors: (state: any, actors: any) => (state.actors = actors)
};

export default {
  state,
  getters,
  actions,
  mutations
};