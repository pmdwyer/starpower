import Vue from 'vue'
import store from './store'
import VueRouter from 'vue-router'

import App from './App.vue'
import Actors from './components/Actors.vue'
import Actor from './components/Actor.vue'
import Movie from './components/Movie.vue'
import Search from './components/Search.vue'

Vue.config.productionTip = false

Vue.use(VueRouter)

const routes = [
  { path: '/actors', component: Actors },
  { path: '/movie/:title', component: Movie },
  { path: '/actor/:name', name: 'actor', component: Actor },
  { path: '/search/:words', component: Search }
]

const router = new VueRouter({
  routes
})

new Vue({
  store,
  router,
  render: h => h(App)
}).$mount('#app')
