<?php
declare(strict_types=1);

use App\Application\Actions\Actor\ListActorsAction;
use App\Application\Actions\Actor\ViewActorAction;
use App\Application\Actions\Movie\ListMoviesAction;
use App\Application\Actions\Movie\ViewMovieAction;
use App\Application\Actions\Search\NameTitleSearchAction;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\App;
use Slim\Interfaces\RouteCollectorProxyInterface as Group;

return function (App $app) {
    $app->options('/{routes:.*}', function (Request $request, Response $response) {
        // CORS Pre-Flight OPTIONS Request Handler
        return $response;
    });

    $app->group('/actor', function (Group $group) {
        $group->get('', ListActorsAction::class);
        $group->get('/{name}', ViewActorAction::class);
    });

    $app->group('/movie', function (Group $group) {
        $group->get('', ListMoviesAction::class);
        $group->get('/{title}', ViewMovieAction::class);
    });

    $app->group('/search', function(Group $group) {
        $group->get('/{words}', NameTitleSearchAction::class);
    });
};
