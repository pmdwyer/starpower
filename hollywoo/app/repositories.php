<?php
declare(strict_types=1);

use App\Domain\Actor\ActorRepository;
use App\Domain\Movie\MovieRepository;
use App\Infrastructure\Persistence\Actor\MariadbActorRepository;
use App\Infrastructure\Persistence\Movie\MariadbMovieRepository;
use DI\ContainerBuilder;

return function (ContainerBuilder $containerBuilder) {
    // Here we map our UserRepository interface to its in memory implementation
    $containerBuilder->addDefinitions([
        ActorRepository::class => \DI\autowire(MariadbActorRepository::class),
        MovieRepository::class => \DI\autowire(MariadbMovieRepository::class),
    ]);
};
