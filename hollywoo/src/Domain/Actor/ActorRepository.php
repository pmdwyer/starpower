<?php
declare(strict_types=1);

namespace App\Domain\Actor;

interface ActorRepository
{
    /**
     * @return Actor[]
     */
    public function getActors(int $page, int $limit): array;

    /**
     * @return string[]
     */
    public function findActorsInMovie(int $mid): array;

    /**
     * @return string[]
     */
    public function findActorNamesLike(string $name): array;

    /**
     * @return Actor
     */
    public function findFirstActorWithName(string $name): Actor;
}
