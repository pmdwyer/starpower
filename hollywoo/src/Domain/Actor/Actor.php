<?php
declare(strict_types=1);

namespace App\Domain\Actor;

use \DateTime;
use JsonSerializable;

class Actor implements JsonSerializable
{
    /**
     * @var int|null
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var DateTime
     */
    private $birthdate;

    /**
     * @var DateTime
     */
    private $deathdate;

    /**
     * @var string
     */
    private $url;

    /**
     * @var float
     */
    private $power;

    /**
     * @var array
     */
    public $movies;

    /**
     * @param int|null  $id
     * @param string    $name
     */
    public function __construct(?int $id, string $name, datetime $bday, ?datetime $dday, string $url, float $power)
    {
        $this->id = $id;
        $this->name = strtolower($name);
        $this->birthdate = $bday;
        $this->deathdate = $dday;
        $this->url = $url;
        $this->power = $power;
        $this->movies = array();
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    public function getBirthday(): DateTime
    {
      return $this->birthdate;
    }

    public function getDeathday(): DateTime
    {
      return $this->deathdate;
    }

    public function getUrl(): string
    {
      return $this->url;
    }

    public function getPower(): float
    {
      return $this->power;
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        return get_object_vars($this);
    }
}
