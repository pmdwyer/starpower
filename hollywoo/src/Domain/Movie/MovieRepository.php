<?php
declare(strict_types=1);

namespace App\Domain\Movie;

interface MovieRepository
{
    /**
     * @return Movie[]
     */
    public function getMovies(int $page, int $limit): array;

    /**
     * @return string[]
     */
    public function findTitlesByActor(int $id): array;

    /**
     * @return string[]
     */
    public function findMovieTitlesLike(string $words): array;

    /**
     * @return Movie
     */
    public function findFilmWithTitle(string $title): Movie;
}
