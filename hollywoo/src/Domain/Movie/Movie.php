<?php
declare(strict_types=1);

namespace App\Domain\Movie;

use \DateTime;
use JsonSerializable;

class Movie implements JsonSerializable
{
    /**
     * @var int|null
     */
    private $id;

    /**
     * @var string
     */
    private $title;

    /**
     * @var DateTime
     */
    private $release_date;

    /**
     * @var string
     */
    private $url;

    /**
     * @var float
     */
    private $rating;

    /**
     * @var int
     */
    private $votes;

    /**
     * @var float
     */
    private $box_office;

    /**
     * @var float
     */
    private $budget;

    /**
     * @var string[]
     */
    public $cast;

    /**
     * @param int|null  $id
     * @param string    $title
     */
    public function __construct(?int $id, string $title, datetime $rday, string $url, float $rating, int $votes, float $box_office, float $budget)
    {
        $this->id = $id;
        $this->title = strtolower($title);
        $this->release_date = $rday;
        $this->url = $url;
        $this->rating = $rating;
        $this->votes = $votes;
        $this->box_office = $box_office;
        $this->budget = $budget;
        $this->cast = array();
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    public function getReleaseDate(): DateTime
    {
      return $this->release_date;
    }

    public function getUrl(): string
    {
      return $this->url;
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        return get_object_vars($this);
    }
}
