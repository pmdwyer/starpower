<?php
declare(strict_types=1);

namespace App\Application\Actions\Movie;

use Psr\Http\Message\ResponseInterface as Response;

class ViewMovieAction extends MovieAction
{
    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
        $title = $this->resolveArg('title');
        $movie = $this->movieRepository->findFilmWithTitle($title);
        $actors = $this->actorRepository->findActorsInMovie($movie->getId());
        $movie->cast = $actors;

        $this->logger->info("Movie titled `${title}` was viewed.");

        return $this->respondWithData($movie);
    }
}
