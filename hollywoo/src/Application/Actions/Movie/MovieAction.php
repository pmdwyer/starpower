<?php
declare(strict_types=1);

namespace App\Application\Actions\Movie;

use App\Application\Actions\Action;
use App\Domain\Actor\ActorRepository;
use App\Domain\Movie\MovieRepository;
use Psr\Log\LoggerInterface;

abstract class MovieAction extends Action
{
    /**
     * @var MovieRepository
     */
    protected $movieRepository;

    /**
     * @var ActorRepository
     */
    protected $actorRepository;

    /**
     * @param LoggerInterface $logger
     * @param MovieRepository  $MovieRepository
     */
    public function __construct(LoggerInterface $logger, MovieRepository $movieRepository, ActorRepository $actorRepository)
    {
        parent::__construct($logger);
        $this->movieRepository = $movieRepository;
        $this->actorRepository = $actorRepository;
    }
}
