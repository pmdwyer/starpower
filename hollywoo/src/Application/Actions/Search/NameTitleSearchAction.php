<?php
declare(strict_types=1);

namespace App\Application\Actions\Search;

use Psr\Http\Message\ResponseInterface as Response;

class NameTitleSearchAction extends SearchAction
{
/**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
      $words = $this->resolveArg('words');
      $people = $this->actorRepository->findActorNamesLike($words);
      $potentPpl = array_map(null, $people, array_fill(0, count($people), 'actor'));
      $movies = $this->movieRepository->findMovieTitlesLike($words);
      $potentMovies = array_map(null, $movies, array_fill(0, count($movies), 'movie'));
      $val = array_merge($potentPpl, $potentMovies);
      return $this->respondWithData($val);
    }
}