<?php
declare(strict_types=1);

namespace App\Application\Actions\Actor;

use Psr\Http\Message\ResponseInterface as Response;

class ListActorsAction extends ActorAction
{
    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
        $page = (isset($_GET['page']) && $_GET['page'] > 0) ? intval($_GET['page']) : 1;
        $limit = isset($_GET['limit']) ? intval($_GET['limit']) : 25;
        $actors = $this->actorRepository->getActors($page, $limit);
        // $actors = array_filter(
        //     $actors, 
        //     function ($a)
        //     {
        //         return $a->getPower() > 0.0;
        //     });
        return $this->respondWithData(array_values($actors));
    }
}
