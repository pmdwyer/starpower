<?php
declare(strict_types=1);

namespace App\Application\Actions\Actor;

use App\Application\Actions\Action;
use App\Domain\Actor\ActorRepository;
use App\Domain\Movie\MovieRepository;
use Psr\Log\LoggerInterface;

abstract class ActorAction extends Action
{
    /**
     * @var ActorRepository
     */
    protected $actorRepository;

    /**
     * @var MovieRepository
     */
    protected $movieRepository;

    /**
     * @param LoggerInterface $logger
     * @param ActorRepository  $ActorRepository
     */
    public function __construct(LoggerInterface $logger, ActorRepository $actorRepository, MovieRepository $movieRepository)
    {
        parent::__construct($logger);
        $this->actorRepository = $actorRepository;
        $this->movieRepository = $movieRepository;
    }
}
