<?php
declare(strict_types=1);

namespace App\Application\Actions\Actor;

use Psr\Http\Message\ResponseInterface as Response;

class ViewActorAction extends ActorAction
{
    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
        $name = $this->resolveArg('name');
        $actor = $this->actorRepository->findFirstActorWithName($name);
        $titles = $this->movieRepository->findTitlesByActor($actor->getId());
        $actor->movies = $titles;

        $this->logger->info("Actor named `${name}` was viewed.");

        return $this->respondWithData($actor);
    }
}
