<?php
declare(strict_types=1);

namespace App\Infrastructure\Persistence\Actor;

use \PDO;
use \DateTime;
use App\Domain\Actor\Actor;
use App\Domain\Actor\ActorNotFoundException;
use App\Domain\Actor\ActorRepository;

class MariadbActorRepository implements ActorRepository
{
    private $pdo;

    public function __construct(PDO $pdo)
    {
      $this->pdo = $pdo;
    }

    /**
     * {@inheritdoc}
     */
    public function getActors(int $page, int $limit): array
    {
      $stmt = $this->pdo->prepare('SELECT * FROM people LIMIT :limit OFFSET :offset');
      $stmt->bindValue(':limit', $limit, PDO::PARAM_INT);
      $stmt->bindValue(':offset', $limit * $page, PDO::PARAM_INT);
      $stmt->execute();
      $actors = array();
      while ($row = $stmt->fetch())
      {
        $actor = $this->makeActor($row);
        array_push($actors, $actor);
      }
      return array_values($actors);
    }

    /**
     * {@inheritdoc}
     */
    public function findActorsInMovie(int $mid): array
    {
      $sql = 'SELECT p.name FROM people AS p JOIN stars_in s ON (p.person_id = s.person) JOIN movies m ON (m.movie_id = s.movie) WHERE s.movie = :id';
      $stmt = $this->pdo->prepare($sql);
      $stmt->bindValue(':id', $mid, PDO::PARAM_INT);
      $stmt->execute();
      $cast = array();
      while ($row = $stmt->fetch())
      {
        $person = str_replace(chr(194).chr(160), ' ', $row[0]);
        array_push($cast, $person);
      }
      return $cast;
    }

    /**
     * {@inheritdoc}
     */
    public function findActorNamesLike(string $name): array
    {
      $name = "%{$name}%";
      $sql = 'SELECT p.name FROM people AS p WHERE p.name LIKE :name';
      $stmt = $this->pdo->prepare($sql);
      $stmt->bindValue(':name', $name, PDO::PARAM_STR);
      $stmt->execute();
      $peeps = array();
      while ($row = $stmt->fetch())
      {
        array_push($peeps, $row[0]);
      }
      return $peeps;
    }

    /**
     * {@inheritdoc}
     */
    public function findFirstActorWithName(string $name): Actor
    {
      $name = "%{$name}%";
      $sql = "SELECT * FROM people WHERE name LIKE :name";
      $stmt = $this->pdo->prepare($sql);
      $stmt->bindValue(':name', $name, PDO::PARAM_STR);
      $stmt->execute();
      $actor = $stmt->fetch(PDO::FETCH_ASSOC);
      if ($actor === false)
      {
        throw new ActorNotFoundException();
      }
      return $this->makeActor($actor);
    }

    private function makeActor($row): Actor
    {
      return new Actor(intval($row['person_id']), $row['name'], new DateTime($row['birthdate']), new DateTime($row['deathdate']), $row['url'], floatval($row['star_rating']));
    }
}
