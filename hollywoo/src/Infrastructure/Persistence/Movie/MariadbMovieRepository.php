<?php
declare(strict_types=1);

namespace App\Infrastructure\Persistence\Movie;

use \PDO;
use \DateTime;
use App\Domain\Movie\Movie;
use App\Domain\Movie\MovieNotFoundException;
use App\Domain\Movie\MovieRepository;

class MariadbMovieRepository implements MovieRepository
{
    private $pdo;

    public function __construct(PDO $pdo)
    {
      $this->pdo = $pdo;
    }

    /**
     * {@inheritdoc}
     */
    public function getMovies(int $page, int $limit): array
    {
      $stmt = $this->pdo->prepare('SELECT * FROM movies LIMIT :limit OFFSET :offset');
      $stmt->bindValue(':limit', $limit, PDO::PARAM_INT);
      $stmt->bindValue(':offset', $limit * $page, PDO::PARAM_INT);
      $stmt->execute();
      $movies = array();
      while ($row = $stmt->fetch())
      {
        $movie = $this->makeMovie($row);
        array_push($movies, $movie);
      }
      return array_values($movies);
    }

    /**
     * {@inheritdoc}
     */
    public function findTitlesByActor(int $id): array
    {
      $sql = 'SELECT m.title FROM movies AS m JOIN stars_in s ON (m.movie_id = s.movie) JOIN people p ON (p.person_id = s.person) WHERE s.person = :id';
      $stmt = $this->pdo->prepare($sql);
      $stmt->bindValue(':id', $id, PDO::PARAM_INT);
      $stmt->execute();
      $titles = array();
      while ($row = $stmt->fetch())
      {
        $title = str_replace(chr(194).chr(160), ' ', $row[0]);
        array_push($titles, $title);
      }
      return $titles;
    }

    /**
     * {@inheritdoc}
     */
    public function findMovieTitlesLike(string $title): array
    {
      $title = "%{$title}%";
      $sql = 'SELECT m.title FROM movies AS m WHERE m.title LIKE :title';
      $stmt = $this->pdo->prepare($sql);
      $stmt->bindValue(':title', $title, PDO::PARAM_STR);
      $stmt->execute();
      $films = array();
      while ($row = $stmt->fetch())
      {
        array_push($films, $row[0]);
      }
      return $films;
    }

    /**
     * {@inheritdoc}
     */
    public function findFilmWithTitle(string $title): Movie
    {
      $title = "%{$title}%";
      $sql = "SELECT * FROM movies WHERE title LIKE :title";
      $stmt = $this->pdo->prepare($sql);
      $stmt->bindValue(':title', $title, PDO::PARAM_STR);
      $stmt->execute();
      $movie = $stmt->fetch(PDO::FETCH_ASSOC);
      if ($movie === false)
      {
        throw new MovieNotFoundException();
      }
      return $this->makeMovie($movie);
    }

    private function makeMovie($row): Movie
    {
      return new Movie(
        intval($row['movie_id']),
        $row['title'],
        new DateTime($row['release_date']),
        $row['url'],
        floatval($row['rating']),
        intval($row['votes']),
        floatval($row['box_office']),
        floatval($row['budget']));
    }
}
