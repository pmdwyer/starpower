﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;

using NLog;
using Newtonsoft.Json;
using MySql.Data.MySqlClient;

using Crawler;
using ImdbCrawler;

namespace radiance
{
  class Program
  {
    private static NLog.Logger s_logger = NLog.LogManager.GetCurrentClassLogger();

    static void Main(string[] args)
    {
      if (args.Length < 2)
      {
        PrintUsage();
        return;
      }

      string config_file = args[1];
      if (!File.Exists(config_file))
      {
        s_logger.Error("Could not locate config file {0}", config_file);
        return;
      }
      // @TODO: check permissions on file
      var config_contents = File.ReadAllText(config_file);
      Config config = JsonConvert.DeserializeObject<Config>(config_contents);
      string connectionStr = string.Format("server={0};database={1};user={2};password={3}", config.DbHost, config.Database, config.DbUser, config.DbPassword);
      MySqlConnection connection = new MySqlConnection(connectionStr);
      PersonStore.Conn = connection;
      
      var people = PersonStore.Instance.GetAll();
      foreach (var person in people)
      {
        IEnumerable<double> ratings = PersonStore.Instance.GetAllMovieRatingsFor(person.Name);
        if (ratings == null || ratings.Count() <= 0)
          continue;
        person.Power = ratings.Average();
        PersonStore.Instance.Update(person);
      }
    }

    private static void PrintUsage()
    {
      Console.WriteLine("usage: radiance <config file>");
    }
  }
}
