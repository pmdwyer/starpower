﻿using System;

namespace Crawler
{
  public interface IDelayPolicy
  {
    TimeSpan Next();
  }
}
