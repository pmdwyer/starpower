﻿using System;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using HtmlAgilityPack;

namespace Crawler
{
  public class CachedPageFetcher : IFetcher
  {
    private static NLog.Logger s_logger = NLog.LogManager.GetCurrentClassLogger();

    private HttpClient m_client = new HttpClient();
    private string m_cache_dir;

    public CachedPageFetcher(string cacheDir)
    {
      if (!Directory.Exists(cacheDir))
      {
        s_logger.Error("Could not find directory {0}", cacheDir);
        throw new DirectoryNotFoundException(cacheDir);
      }
      // Todo: check perms on dir
      m_cache_dir = cacheDir;
      m_client.DefaultRequestHeaders.UserAgent.ParseAdd("Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.105 Safari/537.36 Vivaldi/2.4.1488.38");
    }

    public Page Fetch(Uri url, out bool cacheHit)
    {
      Page page = new Page(url, new HtmlDocument());
      cacheHit = false;
      string file = GetFilename(url);
      string fullpath = Path.Combine(m_cache_dir, file);

      if (!File.Exists(fullpath) || (File.GetLastAccessTime(fullpath) < DateTime.Now.AddDays(-7)))
      {
        s_logger.Info("Fetching page {0}", url);
        var msg = GetPage(url);
        msg.Wait();
        var content = GetContent(msg.Result);
        content.Wait();
        page.doc.LoadHtml(content.Result);
        // Todo: check file perms for writing
        s_logger.Info("Saving page to {0}", fullpath);
        File.WriteAllText(fullpath, page.doc.Text);
        // did we actually write? does it matter?
      }
      else
      {
        cacheHit = true;
        s_logger.Info("Found cached page {0}", fullpath);
        page.doc.LoadHtml(File.ReadAllText(fullpath));
      }
      return page;
    }

    private async Task<HttpResponseMessage> GetPage(Uri url)
    {
      var msgThread = m_client.GetAsync(url);
      var msg = await msgThread;
      return msg;
    }

    private string GetFilename(Uri url)
    {
      char[] invalid_chars = Path.GetInvalidFileNameChars();
      string fname = url.AbsolutePath;
      string new_name = new string(fname.Where((char c) => !invalid_chars.Contains(c)).ToArray());
      new_name += ".html";
      return new_name;
    }

    private async Task<string> GetContent(HttpResponseMessage resp)
    {
      var str = await resp.Content.ReadAsStringAsync();
      return str;
    }
  }
}
