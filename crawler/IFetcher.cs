﻿using HtmlAgilityPack;
using System;

namespace Crawler
{
  public interface IFetcher
  {
    Page Fetch(Uri uri, out bool cacheHit);
  }
}
