﻿using HtmlAgilityPack;
using System;

namespace Crawler
{
  public interface IParser
  {
    bool CanParse(Page page);
    void Parse(Page page);
  }
}