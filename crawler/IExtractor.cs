﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;

namespace Crawler
{
  public interface IExtractor
  {
    IEnumerable<Uri> GetLinks(Page page);
  }
}