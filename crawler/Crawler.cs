﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace Crawler
{
  public class Crawler
  {
    public Crawler(IEnumerable<Uri> seeds, IDelayPolicy gen, IFetcher fetcher, IEnumerable<IExtractor> extractor, IEnumerable<IParser> parsers)
    {
      m_seeds = seeds;
      m_delay = gen;
      m_extractors = extractor;
      m_fetcher = fetcher;
      m_parsers = parsers;
    }

    public void Run()
    {
      Queue<Uri> queue = new Queue<Uri>(m_seeds);
      bool cacheHit = false;
      while (queue.Count > 0)
      {
        Uri url = queue.Dequeue();

        Page page = m_fetcher.Fetch(url, out cacheHit);

        m_extractors.ToList().ForEach(extractor => extractor.GetLinks(page).Where(uri => uri != null).ToList().ForEach(uri => queue.Enqueue(uri)));

        var parser = m_parsers.FirstOrDefault(p => p.CanParse(page));
        if (parser != null)
          parser.Parse(page);

        if (!cacheHit)
          Thread.Sleep(m_delay.Next());
      }
    }

    private IEnumerable<Uri> m_seeds;
    private IDelayPolicy m_delay;
    private IEnumerable<IExtractor> m_extractors;
    private IFetcher m_fetcher;
    private IEnumerable<IParser> m_parsers;
  }
}
