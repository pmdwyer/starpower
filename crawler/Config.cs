﻿namespace Crawler
{
  public class Config
  {
    public string CacheDir
    {
      get; set;
    }

    public string SeedFile
    {
      get; set;
    }

    public string DbHost
    {
      get; set;
    }

    public string Database
    {
      get; set;
    }
    
    public string DbUser
    {
      get; set;
    }

    public string DbPassword
    {
      get; set;
    }
  }
}
