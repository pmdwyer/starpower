﻿using HtmlAgilityPack;
using System;

namespace Crawler
{
  public class Page
  {
    public Page(Uri uri, HtmlDocument doc)
    {
      this.uri = uri;
      this.doc = doc;
    }

    public Uri uri { get; set; }
    public HtmlDocument doc { get; set; }
  }
}
