﻿using System;

namespace Crawler
{
  public class DefaultDelayPolicy : IDelayPolicy
  {
    private static NLog.Logger s_logger = NLog.LogManager.GetCurrentClassLogger();

    public DefaultDelayPolicy(TimeSpan default_delay)
    {
      m_delay = default_delay;
    }

    public TimeSpan Next()
    {
      s_logger.Info("Next delay is {0}", m_delay);
      return m_delay;
    }

    private TimeSpan m_delay;
  }
}
