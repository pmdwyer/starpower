﻿using System;

namespace Crawler
{
  public class RandomDelayPolicy : IDelayPolicy
  {
    private static NLog.Logger s_logger = NLog.LogManager.GetCurrentClassLogger();

    // all in seconds
    private static int Min = 10;
    private static int Max = 20;
    private static int LongMax = 60;
    private static int Repeat = 10;

    private int m_next_long_delay = 5;
    private readonly Random random = new Random();

    public TimeSpan Next()
    {
      int min = Min;
      int max = Max;

      m_next_long_delay--;
      if (m_next_long_delay <= 0)
      {
        min = Max;
        max = LongMax;
        m_next_long_delay = random.Next(1, Repeat);
      }

      int delay = random.Next(min, max);
      s_logger.Info("Next delay is {0}", delay);
      return new TimeSpan(0, 0, delay);
    }
  }
}
