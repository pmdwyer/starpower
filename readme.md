﻿# Todo
## General
  * add mongo db instance
  * docker compose for dev

## Database
  * separate out ratings
### People
  * add a enum to person for job type?
  * last fetch date / freshness
### Movies
  * last fetch date / freshness

## Crawler
  * validate db connection string
  * verify json config format
  * check cache and timestamp for this link
  * separate out fetcher
  * add mongo db page cache
  * composeable link extractors
  * url normaliz(ation|er)
  * sql cmd runner util that wraps open, try, finally, close
  * webdriver (selenium?)
    * onclick=toggleFilmoCategory(this);
  * web proxy
  * only add movie / person when parsing their page
  * static class for queries
    * query utils also

## Stats Calc
  * different algorithms
    * bayesian
  * consider new tables for rating values

## Web backend

## Web UI