﻿using System;
using System.Collections.Generic;
using System.IO;

using HtmlAgilityPack;
using MySql.Data.MySqlClient;
using Newtonsoft.Json;

using ImdbCrawler;
using Crawler;

namespace StarPower
{
  class Program
  {
    private static NLog.Logger s_logger = NLog.LogManager.GetCurrentClassLogger();

    public static void Main(string[] args)
    {
      if (args.Length < 1)
      {
        PrintUsage();
        return;
      }
      string config_file = args[0];
      if (!File.Exists(config_file))
      {
        Console.WriteLine("Could not locate config file {}", config_file);
        return;
      }
      // @TODO: check permissions on file
      var config_contents = File.ReadAllText(config_file);

      // @TODO: verify json format
      Config config = JsonConvert.DeserializeObject<Config>(config_contents);
      string connectionStr = string.Format("server={0};database={1};user={2};password={3}", config.DbHost, config.Database, config.DbUser, config.DbPassword);
      MySqlConnection connection = new MySqlConnection(connectionStr);
      PersonStore.Conn = connection;
      MovieStore.Conn = connection;

      Uri knUri = new Uri("https://www.imdb.com/title/tt5439796/?ref_=nm_flmg_act_10");
      HtmlDocument knDoc = new HtmlDocument();
      knDoc.Load(Path.Join(config.CacheDir,"titlett5439796.html"));
      Page knivesOut = new Page(knUri, knDoc);

      ImdbPersonLinkExtractor personLinks = new ImdbPersonLinkExtractor();
      IEnumerable<Uri> personUris = personLinks.GetLinks(knivesOut);
      ImdbMoviePageParser mpageParser = new ImdbMoviePageParser();
      mpageParser.Parse(knivesOut);

      // Uri ccUri = new Uri("https://www.imdb.com/name/nm0185819/?ref_=nv_sr_srsg_0");
      // HtmlDocument ccDoc = new HtmlDocument();
      // ccDoc.Load(Path.Join(config.CacheDir, "Daniel Craig - IMDb.html"));
      // Page charlieChaplin = new Page(ccUri, ccDoc);

      // ImdbMovieLinkExtractor movieLinks = new ImdbMovieLinkExtractor();
      // IEnumerable<Uri> movieUris = movieLinks.GetLinks(charlieChaplin);
      // ImdbPersonPageParser ppageParser = new ImdbPersonPageParser();
      // ppageParser.Parse(charlieChaplin);
    }

    private static void PrintUsage()
    {
      Console.WriteLine("usage: starpower <config file>");
    }
  }
}